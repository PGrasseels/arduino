/*
1) D38B Dallas => IN PIN D4 = 4
2) Pin central dallas => résistance(4,8) + VCC

3) Photorésistance => IN PIN A0 = 0
4) Pin in => GRD + Résistance 10K --> A0


*/

#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 2 on the Arduino
#define TEMPERATURE_PIN 4

int photocellPin = 0; // the cell and 10K pulldown are connected to a0
int photocellReading;

OneWire oneWire(TEMPERATURE_PIN);
DallasTemperature sensors(&oneWire);

void setup(void)
{
  // start serial port
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");
  pinMode(TEMPERATURE_PIN, INPUT);
  // Start up the library
  sensors.begin();
}

void loop(void)
{ 
  photocellReading = analogRead(photocellPin);
  Serial.print("Analog reading = ");
  Serial.println(photocellReading);
  sensors.requestTemperatures();
  Serial.print("Temp reading = ");
  Serial.println(sensors.getTempCByIndex(0)); 
  delay(2500);
}
